using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheDigitalTailor.Models
{
	public class Booking 
	{
		[Key]
		public int Id { get; set; }

		public DateTime StartDateTime { get; set; }
		public DateTime? EndDateTime { get; set; }
		public bool AllDay { get; set; }

		public string Title { get; set; }

		public ICollection<int> FuneralIds { get; set; }
	}
}
